# Yaml dOcUment and Requirement System

Creating documents. In control.

# Purpose

The main purpose of this project is to create a standardized format to allow
development of Requirement and Test Driven Development of software.

The rationale is that there is a lack of Open Source tools that are focused on
delivering DO-178C compatible software.

However, it is, of course, smart to write _any_ software based on requirements
and tests rather that "happy hacking" for many reasons, a few being:
* Safety
* Quality
* Control
* Cost

# Workflow

Write requirements:
* **What** **_shall_** the software do.
* Include a _rationale_ as to explain **why**.
* **Inspect** that it is correct.

Write a design:
* **How** does the requirements split into functions.
* **Where** is each function realized.
* **Inspect** that it is correct.

Write tests:
* **Verify** that the code executes correctly.
* **Inspect** that it is correct.

Write implementation:
* **Code**
* **Test**
* **Inspect** that it is correct.

## Rinse and repeat

All steps above **_shall_** feedback information to the step before!

# Layout

When creating software that is safety critical, the life cycle of all of its
parts must be considered. Especially time, which in some cases can last for many
**_decades_**.

This is another reason why Open Source should be used. Companies rise and fall
on a much shorter time span than these kind of software.

For the same reason, binary file formats should also be avoided. Human readable,
and therefore scriptable, guarantee that the data can be accessed over time.

Standardized formats are preferred. The main purpose of this project is to
provide an Open Standard for human readable requirements and documentation for
software development.

## File formats

All files **_shall_** be in the YAML format.

## Programs

Programs for verifying and manipulating the files will be provided.
To fulfill different needs of simplicity, commonality and speed, three flavors
that all _do the same job_ will be created:
* **bash** - Available almost anywhere. No dependencies needs to be installed.
* **python** - Easy to install. JIT, so faster than bash.
* **rust** - Compiled. Fastest. Requires installation, building, etc.

## Dependencies

### git

It will be used as the main way of keeping track of files, versions, etc.

**Rationale**

Open Source, efficient for text based files.

### LaTeX

It will be used as the main formatting language to create PDF:s.

**Rationale**

Open Source, has been around for decades and is likely to stay.
