#!/usr/bin/env bash

[[ $_ != $0 ]] && echo "Script is sourced. You must use run it as a subshell. Exiting." && return 1

if ! [[ -f "programs/bash/lib/scripts/common/shell.sh" ]]; then
  echo "Cannot find 'programs/bash/lib/scripts/common/shell.sh'."
  echo "Make sure the submodule 'scripts' is initialized."
  echo "Stopping."
  return 2
fi
source programs/bash/lib/scripts/common/shell.sh

# Run document validation
printf "${MAGENTA}"
message "This script will validate all files in ${GREEN}doc${MAGENTA} folder
against the ${GREEN}schema${MAGENTA} files."
printf "${NC}"
START=$(date +%s.%N | tr 'N' '0')
yours validate doc standards/schemas
STOP=$(date +%s.%N | tr 'N' '0')
DIFF=$(subtract "$STOP" "$START")
TIME=$(format_time "${DIFF}")
printf "${MAGENTA}"
message "All ${green}passed!${MAGENTA} in ${TIME}."
printf "${NC}"
