// General imports
use structopt::StructOpt;
use std::io::BufReader;
use std::fs::File;
use failure::ResultExt;
use exitfailure::ExitFailure;
use std::path::PathBuf;

// Local imports
mod libs;

// Setup logging
#[macro_use]
extern crate log;
extern crate env_logger;

/// Search for a pattern in a file and display the lines that contain it.
#[derive(StructOpt)]
#[derive(Debug)]
enum Cli {
    // /// The pattern to look for
    // pattern: String,
    // /// The path to the file to read
    // #[structopt(parse(from_os_str))]
    // path: std::path::PathBuf,

    /// Add a new section
    #[structopt(name = "add")]
    Add {
        #[structopt(short = "i")]
        interactive: bool,
        #[structopt(short = "p")]
        patch: bool,
        #[structopt(parse(from_os_str))]
        files: Vec<PathBuf>
    },
    #[structopt(name = "fetch")]
    Fetch {
        #[structopt(long = "dry-run")]
        dry_run: bool,
        #[structopt(long = "all")]
        all: bool,
        repository: Option<String>
    },
    #[structopt(name = "commit")]
    Commit {
        #[structopt(short = "m")]
        message: Option<String>,
        #[structopt(short = "a")]
        all: bool
    }
}

fn main() -> Result<(), ExitFailure> {
    env_logger::init();
    info!("Starting up yours.");
    // Read arguments
    let args = Cli::from_args();

    println!("{:#?}", args);
    // // Open the file
    // let f = File::open(&args.path)
    //     .with_context(|_| format!("could not read file `{:?}`", &args.path))?;
    // // Buffer reader
    // let reader = BufReader::new(f);

    // Find pattern
    // well::find_matches(&reader, &args.pattern, &mut std::io::stdout());


    // // Loop over all lines
    // for line in reader.lines() {
    //     // Get the data from the result object
    //     let data = line.unwrap();
    //     // Find the pattern
    //     if data.contains(&args.pattern) {
    //         println!("{}", data);
    //     }
    // }
    // Exit nicely
    info!("All done!");
    Ok(())
}
