#!/usr/bin/env bash

# Validates yaml files against schema files.


function validate() {
  # Execute validation.
  local docs=${1}
  local schemas=${2}
}

function _validate_file() {
  local file=${1}
  local schema=${2}
}
