#!/usr/bin/env bash

# Get the folder name of _this_ script
SRCDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
# Get common shell codes
#source "${SRCDIR}/../lib/scripts/common/shell.sh"
export PATH="${SRCDIR}/../../lib/scripts/common/:${SRCDIR}/../lib/:$PATH"
source "validate.sh"
source "shell.sh"

# Info
usage="yours Yaml dOcUment and Requirement System executable.
Usage:  yours [options] <command> <item>...
Options:
        -h, --help                        Display this help and exit.
Commands:
        validate <folder> <schema_folder> Validate file against its schema.

The program will perform stuff Yaml dOcUment and Requirement System.
"

registry="registry.hub.docker.com"

#Check arguments
while [[ $# -gt 0 ]]; do
  key="$1"
  case $key in
    -h|--help)
      printf "${NC}${usage}\n"
      exit 0
      ;;
    validate)
      folder="$2"
      schema_folder="$3"
      shift
      shift
      shift
      ;;
    -t|--tag)
      tag="$2"
      shift
      shift
      ;;
    -*)
      printf "${CYAN}"
      printf "Unknown command given. (${1})\n"
      printf "${RED}"
      printf "Exiting.\n"
      printf "${NC}"
      printf "${NC}${usage}\n"
      exit 2
      ;;
    *)
      printf "${CYAN}"
      printf "Unknown command given. (${1})\n"
      printf "${RED}"
      printf "Exiting.\n"
      printf "${NC}"
      printf "${NC}${usage}\n"
      exit 2
      ;;
  esac
done

printf "Doing nothing yet."
