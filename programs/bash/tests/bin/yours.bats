#!/usr/bin/env bats
load ../helpers/bin_helper

@test "yours::::Testing that no arguments fails." {
  run yours.sh
  expected=$(printf "Doing nothing yet.")
  echo "expected = '${expected[@]}'"
  echo "Output   = '${output[@]}'"
  echo "Status   = '${status}'"
  [ "$status" -eq 0 ]
  [ "$output" = "$expected" ]
}
