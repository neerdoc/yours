#!/usr/bin/env bats
load ../helpers/lib_helper

@test "validate::_validate::Testing all_good.yaml validates correctly." {
  run _validate_file 1 2
  expected=$(printf "Doing nothing yet.")
  echo "expected = '${expected[@]}'"
  echo "Output   = '${output[@]}'"
  echo "Status   = '${status}'"
  [ "$status" -eq 0 ]
  [ "$output" = "$expected" ]
}
