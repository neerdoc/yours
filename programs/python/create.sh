#!/usr/bin/env bash

source ../bash/lib/scripts/common/shell.sh

printf "${MAGENTA}"
message "Creating python3 virtual environment."
printf "${NC}"
python3 -m venv .pyenv

# Install Yamale
printf "${MAGENTA}"
message "Installing ${YELLOW}Yamale${MAGENTA} requirements."
printf "${NC}"

source .pyenv/bin/activate
pip install -r requirements-Yamale.txt

printf "${MAGENTA}"
message "Running ${YELLOW}Yamale${MAGENTA} test suite."
printf "${NC}"

mkdir -p .pyenv/testing/
pushd .pyenv/testing/
git clone https://github.com/23andMe/Yamale.git
cd Yamale
py.test --cov yamale --cov-report term-missing yamale
popd

# Install yours
printf "${MAGENTA}"
message "Installing ${YELLOW}yours${MAGENTA} requirements."
printf "${NC}"

pushd yours
python setup.py install

printf "${MAGENTA}"
message "Running ${YELLOW}yours${MAGENTA} test suite."
printf "${NC}"
py.test --cov yours --cov-report term-missing yours

popd

# Done
deactivate

printf "${CYAN}"
message "To start the python virtual environment, write:
§
${blue}source .pyenv/bin/activate${CYAN}
§
§
When done, to exit, write:
§
${blue}deactivate${CYAN}
§
§
To remove virtual environment, write:
§
${blue}rm -fr .pyenv${CYAN}"
printf "${NC}"
