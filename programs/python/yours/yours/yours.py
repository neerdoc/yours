#!/usr/bin/env python3
"""
Module Docstring.
"""

__author__ = "Gustav Johansson"
__version__ = "0.0.1"
__license__ = "MIT"

import argparse
import sys


def main():
    """
    Main entry point of the app.
    This is executed when run from the command line.
    """
    parser = argparse.ArgumentParser()

    # Required positional argument
    parser.add_argument("command",
                        nargs=argparse.REMAINDER,
                        help="Command to execute."
                        )

    # Specify output of "--version"
    parser.add_argument(
        "--version",
        action="version",
        version="%(prog)s (version {version})".format(version=__version__))

    args = parser.parse_args()

    if len(args.command) == 0:
        print("Nothing to do.")
        sys.exit(0)
    if args.command[0] == "validate":
        # Sanity checks
        usage = """
Usage:
yours validate <folder_to_check> <schema_folder>
"""
        if len(args.command) != 3:
            print("Bad number of arguments for command 'validate'.")
            print(usage)
            sys.exit(1)
        from os.path import exists
        if not exists(args.command[1]):
            print("The <folder_to_check> (%s) does not exists." % args.command[1])
            print(usage)
            sys.exit(1)
        elif not exists(args.command[2]):
            print("The <schema_folder> (%s) does not exists." % args.command[2])
            print(usage)
            sys.exit(1)
        from yours.lib.validate import validate
        validate(args.command[1], args.command[2])
    else:
        print("Unknown command given.")
        print("Valid commands are:")
        print("    validate <folder> <schema_folder>")


if __name__ == "__main__":
    main()
