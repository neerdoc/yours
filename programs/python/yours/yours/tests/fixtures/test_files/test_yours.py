"""
Test suite for the file 'yours.py' that need fixtures.
"""
import pytest

@pytest.mark.usefixtures("test_dir")
class TestYamlFiles:
    """
    Class to ensure that the fixtures are setup correctly for all test
    functions.
    """
    def test_command_validate_folder_good(self, script_runner):
        return # Not working yet!
        "Test that command call 'validate' correctly validates a folder against schema folder."
        result = script_runner.run(
            'yours',
            'validate',
            'test_doc_good',
            'test_schema'
            )
        assert result.success
        assert result.stdout == ''
        assert result.stderr == ''


    def test_command_validate_folder_bad(self, script_runner):
        """
        Test that command call 'validate' correctly fails to validate a folder
        of bad yaml files against schema folder.
        """
        return # Not working yet!
        result = script_runner.run('yours', 'validate', 'test_doc_bad', 'test_schema')
        assert not result.success
        assert result.stdout == ''
        assert result.stderr == ''
