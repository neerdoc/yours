"""
Test suite for the file 'validate.py' that need fixtures.
"""
from os import path
import pytest
from yours.lib.validate import _validate_file


@pytest.mark.usefixtures("test_dir")
def test_validate_file_good(validate_file):
    """
    Validate all schema files found in 'yamale_tests' against the 'good' version
    of test data files included in the same folder.
    """
    schema_file = path.join('yamale_tests', validate_file)
    good_file = path.join(
        'yamale_tests',
        '%s_good.yaml' % path.splitext(validate_file)[0])
    assert _validate_file(good_file, schema_file) is None


@pytest.mark.usefixtures("test_dir")
def test_validate_file_bad(validate_file):
    """
    Validate all schema files found in 'yamale_tests' against the 'bad' version
    of test data files included in the same folder.
    """
    schema_file = path.join('yamale_tests', validate_file)
    bad_file = path.join(
        'yamale_tests',
        '%s_bad.yaml' % path.splitext(validate_file)[0])
    with pytest.raises(Exception) as e_info:
        _validate_file(bad_file, schema_file)
    if "Error validating data %s with schema %s" % (bad_file, schema_file) in str(e_info):
        assert True
    else:
        print(e_info)
        print("schema = %s" % schema_file)
        print("bad    = %s" % bad_file)
        assert False
