"""
Fixtures that are used to setup and teardown yaml tests.
"""

import os
import pytest


@pytest.fixture
def test_dir():
    "Change the folder to the 'tests' folder."
    old_cwd = os.getcwd()
    data_path = os.path.join(
        old_cwd,
        '..',
        '..',
        '..',
        'tests'
        )
    os.chdir(data_path)
    yield
    os.chdir(old_cwd)


def pytest_generate_tests(metafunc):
    """
    Generate a test parameter from all files in the 'yamale_tests' folder.
    """
    if "validate_file" in metafunc.fixturenames:
        # Create the list
        old_cwd = os.getcwd()
        data_path = os.path.join(
            old_cwd,
            '..',
            '..',
            '..',
            'tests',
            'yamale_tests'
            )
        schema_list = []
        for entry in os.scandir(data_path):
            # Only use schema files.
            if "bad" not in entry.name and "good" not in entry.name and '.yaml' in entry.name:
                schema_list.append(entry.name)
        metafunc.parametrize("validate_file", schema_list, indirect=True)


@pytest.fixture
def validate_file(request):
    "Return the generated parameter to the test function."
    return request.param
