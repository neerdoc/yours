"""
Test suite for the file 'yours.py'.
"""


def test_yours_empty(script_runner):
    "Test that empty call works."
    result = script_runner.run('yours')
    assert result.success
    assert result.stdout == 'Nothing to do.\n'
    assert result.stderr == ''


def test_yours_version(script_runner):
    "Test that --version call works."
    result = script_runner.run('yours', '--version')
    assert result.success
    assert result.stdout == 'yours (version 0.0.1)\n'
    assert result.stderr == ''


def test_command_validate_no_argument(script_runner):
    "Test that command call 'validate' fails if no argument is supplied."
    result = script_runner.run('yours', 'validate')
    assert not result.success
    assert result.stdout == """Bad number of arguments for command 'validate'.

Usage:
yours validate <folder_to_check> <schema_folder>

"""
    assert result.stderr == ''


def test_command_validate_one_argument(script_runner):
    "Test that command call 'validate' fails if only one argument is supplied."
    result = script_runner.run('yours', 'validate', 'test_doc')
    assert not result.success
    assert result.stdout == """Bad number of arguments for command 'validate'.

Usage:
yours validate <folder_to_check> <schema_folder>

"""
    assert result.stderr == ''


def test_command_validate_three_arguments(script_runner):
    "Test that command call 'validate' fails if more than two arguments are supplied."
    result = script_runner.run('yours', 'validate', 'test_doc', 'test_schema', 'extra_argument')
    assert not result.success
    assert result.stdout == """Bad number of arguments for command 'validate'.

Usage:
yours validate <folder_to_check> <schema_folder>

"""
    assert result.stderr == ''
