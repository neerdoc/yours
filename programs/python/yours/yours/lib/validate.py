"""Validates yaml files against schema files."""
from os import path, scandir
from yamale import yamale
from yaml import safe_load


def validate(docs, schemas):
    """Execute validation."""
    # Iterate over all files in folders.
    schema_list = []
    for entry in scandir(schemas):
        # Only use yaml files.
        if '.yaml' in entry.name:
            schema_list.append(path.splitext(entry.name)[0])
    for entry in scandir(docs):
        # Only use yaml files.
        if '.yaml' in entry.name:
            # Check the 'schema' item first
            found_schema = _get_schema(path.join(docs, entry.name))
            if not found_schema:
                raise KeyError("Could not find the 'schema' key in the file '%s'." %
                               path.join(docs, entry.name))
            if not found_schema in schema_list:
                raise AttributeError("Could not find the schema '%s' found in the file '%s'." %
                                     (found_schema, path.join(docs, entry.name)))
            # Now validate the file against the found schema
            _validate_file(
                path.join(docs, entry.name),
                path.join(schemas, "%s.yaml" % found_schema)
                )


def _validate_file(file, schema):
    """
    Validate file against the schema. Throws a ValueError if data is invalid.
    """
    try:
        schema = yamale.make_schema(schema)
        data = yamale.make_data(file)
        yamale.validate(schema, data, strict=True)
    except Exception as error:
        print("Exception caught in function '%s' when processing file '%s' against schema '%s'" %
              ('_validate_file', file, schema))
        raise error


def _get_schema(file_name):
    """
    Return the value of the key 'schema' from the supplied file. Returns None
    if key is not found.
    """
    with open(file_name, 'r') as stream:
        data = safe_load(stream)
    if 'schema' in data.keys():
        return data['schema']
    return None
