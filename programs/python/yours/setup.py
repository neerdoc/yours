"""
Setup file for 'yours' executable.
"""
from setuptools import setup, find_packages

README = open('README.md', encoding='utf-8').read()
LICENSE = open('LICENSE', encoding='utf-8').read()

setup(
    name='yours',
    version='0.0.1',
    url='https://',
    author='Gustav Johansson',
    author_email='gustav@neer.se',
    description='Yaml dOcUment and Requirement System',
    long_description=README,
    long_description_content_type='text/markdown',
    license='MIT',
    packages=find_packages(),
    include_package_data=True,
    install_requires=[
        'pytest==5.4.1',
        'pytest-console-scripts==0.2.0',
        'pytest-cov==2.8.1',
        'pyyaml==5.3.1',
        'yamale==2.0.1'
        ],
    entry_points={
        'console_scripts': ['yours=yours.yours:main'],
    },
    classifiers=[
        'Development Status :: 1 - Reaalllly new!',
        'Intended Audience :: Developers',
        'Operating System :: OS Independent',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
    ]
)
