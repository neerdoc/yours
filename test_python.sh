#!/usr/bin/env bash

[[ $_ != $0 ]] && echo "Script is sourced. You must use run it as a subshell. Exiting." && return 1

if ! [[ -f "programs/bash/lib/scripts/common/shell.sh" ]]; then
  echo "Cannot find 'programs/bash/lib/scripts/common/shell.sh'."
  echo "Make sure the submodule 'scripts' is initialized."
  echo "Stopping."
  return 2
fi
source programs/bash/lib/scripts/common/shell.sh

# Inform the user
printf "${MAGENTA}"
message "This script will execute the suite for the ${YELLOW}python${MAGENTA} version
of 'yours'."
printf "${NC}"

# Reset environment!
if [[ -d "programs/python/.pyenv" ]]; then
  rm -fr "programs/python/.pyenv"
fi
pushd programs/python/
./create.sh
popd

# Initialize bash version
source init_python.sh

# Execute test suite
pushd programs/python/yours
py.test --cov yours --cov-report term-missing yours
popd

# Run common tests
./test_common.sh

# All done
deactivate
