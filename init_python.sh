#!/usr/bin/env bash

[[ $_ = $0 ]] && echo "Script is a subshell. You must use 'source'. Exiting." && exit 1

if ! [[ -f "programs/bash/lib/scripts/common/shell.sh" ]]; then
  echo "Cannot find 'programs/bash/lib/scripts/common/shell.sh'."
  echo "Make sure the submodule 'scripts' is initialized."
  echo "Stopping."
  return 2
fi
if ! [[ -f "programs/python/.pyenv/bin/activate" ]]; then
  echo "Cannot find 'programs/python/.pyenv/bin/activate'."
  echo "Make sure the python environment has been created. I.e., execute:"
  echo "'pushd programs/python && ./create.sh && popd'"
  echo "Stopping."
  return 3
fi

source programs/bash/lib/scripts/common/shell.sh
source programs/python/.pyenv/bin/activate

# Inform the user
printf "${MAGENTA}"
message "${GREEN}yours${MAGENTA} is now initialized in ${yellow}python${MAGENTA} mode.
§
Try ${cyan}yours -h${MAGENTA} for for information on what commands are available.
§
Use ${cyan}deactivate${MAGENTA} to exit."
printf "${NC}"
