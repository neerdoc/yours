#!/usr/bin/env bash

[[ $_ = $0 ]] && echo "Script is a subshell. You must use 'source'. Exiting." && exit 1

if ! [[ -f "programs/bash/lib/scripts/common/shell.sh" ]]; then
  echo "Cannot find 'programs/bash/lib/scripts/common/shell.sh'."
  echo "Make sure the submodule 'scripts' is initialized."
  echo "Stopping."
  return 2
fi

source programs/bash/lib/scripts/common/shell.sh
deactivate () {
    # reset old environment variables
    if [ -n "${_OLD_VIRTUAL_PATH:-}" ] ; then
        PATH="${_OLD_VIRTUAL_PATH:-}"
        export PATH
        unset _OLD_VIRTUAL_PATH
    fi
    # This should detect bash and zsh, which have a hash command that must
    # be called to get it to forget past commands.  Without forgetting
    # past commands the $PATH changes we made may not be respected
    if [ -n "${BASH:-}" -o -n "${ZSH_VERSION:-}" ] ; then
        hash -r
    fi

    if [ -n "${_OLD_VIRTUAL_PS1:-}" ] ; then
        PS1="${_OLD_VIRTUAL_PS1:-}"
        export PS1
        unset _OLD_VIRTUAL_PS1
    fi

    if [ ! "${1:-}" = "nondestructive" ] ; then
    # Self destruct!
        unset -f deactivate
        return
    fi
}

# unset irrelevant variables
deactivate nondestructive

_OLD_VIRTUAL_PATH="$PATH"
PATH="programs/bash/yours/bin:$PATH"
export PATH

if [ -z "${VIRTUAL_ENV_DISABLE_PROMPT:-}" ] ; then
    _OLD_VIRTUAL_PS1="${PS1:-}"
    if [ "x(yours) " != x ] ; then
	PS1="(yours) ${PS1:-}"
    else
    if [ "`basename \"yours\"`" = "__" ] ; then
        # special case for Aspen magic directories
        # see http://www.zetadev.com/software/aspen/
        PS1="[`basename \`dirname \"yours\"\``] $PS1"
    else
        PS1="(`basename \"yours\"`)$PS1"
    fi
    fi
    export PS1
fi

# This should detect bash and zsh, which have a hash command that must
# be called to get it to forget past commands.  Without forgetting
# past commands the $PATH changes we made may not be respected
if [ -n "${BASH:-}" -o -n "${ZSH_VERSION:-}" ] ; then
    hash -r
fi

# Inform the user
printf "${MAGENTA}"
message "${GREEN}yours${MAGENTA} is now initialized in ${yellow}bash${MAGENTA} mode.
§
Try ${cyan}yours -h${MAGENTA} for for information on what commands are available.
§
Use ${cyan}deactivate${MAGENTA} to exit."
printf "${NC}"
